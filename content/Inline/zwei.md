Title: 臺中市的地標？內嵌！（其實是在破壞Demo機的部份）--關掉15000的內嵌
Date: 2020-08-10 09:10:40
Author: TingChia
tags:MD





## 台中市的地標地圖(以2016為例)
	`其實原本有弄2016 2017 2018 2019的，但是一直當機跑不出來QQ`

- 臺中市地標（2016）--742Kb(共15000筆左右)  

- 臺中市地標（取重要地標）（2016）--504Kb(共10000筆左右)  
<iframe width="700" height="400" src="https://demo.depositar.io/dataset/taichunglandmark_twd97_2016/resource/c9ee9f32-44e5-4055-b3a1-45012937e8cf/view/69df64e5-cf46-4389-a37b-4ea2a5ac29c4" frameBorder="0"></iframe>

**系統的極限為15000筆左右資料，10000筆以上時，跑速會開始變慢，並且當機頻率增加**

## 台北市的行道樹分佈
- 臺北市行道樹分佈
	- 因為80000多筆，直接當機QQ（3.12MB）
	- 不知道為什麼.csv預覽不出來？
	
	- 黃椰子分佈

- 台北市樹穴分佈
	- 一樣也當機(80000筆不爆掉才怪)(863Kb)
	- 所以改成來看.csv檔吧
	

## 福德坑空拍結果
- kml
	- 原本
		- 聽說demo機跑不出來，但是正式網站可以
		<iframe width="700" height="400" src="https://demo.depositar.io/dataset/82a55/resource/bb8245a2-1946-490f-86b2-533a6affba1a/view/d009ffaa-93a5-400c-af73-96856c1f1b56" frameBorder="0"></iframe>
	- 後來
		- 向量的可以，但是raster image不行
		- 向量（以人孔蓋為例）
		<iframe width="700" height="400" src="https://demo.depositar.io/dataset/59efb/resource/41d07df0-7de2-4b2f-9290-784379f4cb72/view/e414fc38-2acc-4c6d-8b69-d608209fcdf4" frameBorder="0"></iframe>
		- raster image
		<iframe width="700" height="400" src="https://demo.depositar.io/dataset/82a55/resource/bb8245a2-1946-490f-86b2-533a6affba1a/view/d009ffaa-93a5-400c-af73-96856c1f1b56" frameBorder="0"></iframe>
- kmz  
`warning`
>> 此資料沒有可用的檢視。  
>> 沒有看到您預期的檢視嗎？ 點擊此處獲得更多資訊  
>> 這裡列出一些可能造成無法檢視的原因：  
>> 沒有適用於此資料的檢視  
>> 系統管理者可能並未啟用相關的檢視擴充套件  
>> 若此檢視需要 DataStore 支援，則可能為 DataStore 擴充套件未啟用、資料並未上傳至 DataStore，或DataStore 上傳作業尚未完成
